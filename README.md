# About pages

This is where the code for [my website](https://hamborgr.codeberg.page) is hosted.
The website was created from scratch to try out and learn something new. It was made purely for fun and as a project with the intention to share my experiences regarding things related to technology and digital privacy from which I hope some people and myself might learn a few things and enjoy the content that will be posted here.

If you need to contact me because of a typo, bad or misleading information, feedback or anything else, feel free to use the contact information on [https://hamborgr.codeberg.page/AboutMe.html](https://hamborgr.codeberg.page/AboutMe.html)

Lastly, this site was generally tested with Firefox and due to the difference of Chromium's rendering engine you may have a broken experience viewing my site on a Chromium-based browser. So for the best experience I would highly recommend using anything based on Firefox.
(A suggestion on how or a commit to fix this would also be very welcome)